
% Initial Approach to processing this data.


function window_claps = process_long(data,fs, window_offset, should_plot)

CLAP_THRESHOLD = 0.01;
still_clapping_offset = 0.02;

NFFT = length(data);


% Plot original Signal

if should_plot
    subplot(4,1,1);

    % TODO Fix Scale
    t = 0:1/fs:(length(data)-1)/fs;
    plot(t, data);
    title('Original Signal');
    xlabel('time(s)');
    ylabel('Voltage(V)');
end

% Remove the 0 Hz Frequency Component
Voltage = data - mean(data);


% Convert to frequency Domain
y = fft(Voltage, NFFT);
faxis = (0:(NFFT/2)-1)/(NFFT/2)*(fs/2);

if should_plot
    subplot(4,1,2);
    plot(faxis, abs(y(1:(NFFT/2))));
    title('FFT of original Signal');
    xlabel('Frequency (Hz)');
    ylabel('Magnitude');
end

% Band Pass frequencies
fpass1 = 750;
fstop1 = 950;

fpass2 = 1450;
fstop2 = 1900;

npass1 = round((fpass1/fs) * NFFT);
nstop1 = round((fstop1/fs) * NFFT);

npass2 = round((fpass2/fs) * NFFT);
nstop2 = round((fstop2/fs) * NFFT);

Llow1 = 0 * y(1:npass1-1);            % pass the signal to an indeal band-pass filter
Lhigh1 = 1 * y(npass1:nstop1-1);
Llow2 = 0 * y(nstop1:npass2-1);
Lhigh2 = 1 * y(npass2:nstop2-1);
Llow3 = 0 * y(nstop2:NFFT);

%Ymod = [Llow1 Lhigh1 Llow2 Lhigh2 Llow3];   % reconstruct the fragment
Ymod = [Llow1;Lhigh1;Llow2;Lhigh2;Llow3];

if should_plot
    subplot(4,1,3);        % plot signal after passing through band-pass filter in freq domain
    plot(faxis, abs(Ymod(1:(NFFT/2))));
    title('Bandpass Filter');
    xlabel('Frequency (Hz)');
    ylabel('Magnitude');
end


Vrec = abs(real(ifft(Ymod)));      % convert signal back to time domain

t = 0:1/fs:(length(Vrec)-1)/fs;

if should_plot
    subplot(4,1,4);                   % plot signal after filtering in time domain
    plot(t,Vrec);
    axis([0 NFFT/fs -0.001 0.05]);
    title('Signal after filtering');
    xlabel('time (s)');
    ylabel('Voltage (V)');
end

current_clap_time = 0;
last_clap_time = 0;
window_claps = [];
for i=1:length(Vrec)
  value = Vrec(i);
  
  % This be the core buisness logic for making sure we only detect a single
  % clap, it uses the CLAP_THRESHOLD to make sure we don't double count a
  % clap. This introduces a tiny chance we miss a clap in the first
  % CLAP_THRESHOLD of the window.
  
  if value > CLAP_THRESHOLD
      current_clap_time = t(i);
  end
  
  % Detect new clap
  if current_clap_time - last_clap_time > still_clapping_offset
      window_claps = [window_claps (window_offset + current_clap_time)]; %#ok<AGROW>
  end
  
  last_clap_time = current_clap_time;
end

end

