clear all;
clear;
close all;

global p;
global f;
global n;
p = 0; 
f = 0;

saved_data_loader

global fs
fs = 41100;

p_total = 0;
f_total = 0;

test(no_claps,[0],'no_claps');
test(no_claps2,[0],'no_claps2');

p_nothing = p; p_total = p_total + p; p = 0;
f_nothing = f; f_total = f_total + f; f = 0;

test(single_clap,[1],'single_clap');
test(single_clap2,[1],'single_clap2');
test(single_clap3,[1],'single_clap3');

p_single = p; p_total = p_total + p; p = 0;
f_single = f; f_total = f_total + f; f = 0;

test(double_clap,[2],'double_clap');
test(double_clap2,[2],'double_clap2');
test(double_clap3,[2],'double_clap3');
test(double_clap4,[2],'double_clap4');

p_double = p; p_total = p_total + p; p = 0;
f_double = f; f_total = f_total + f; f = 0;

test(multiple_off_off_off, [2,2,2],'multiple_off_off_off')
test(multiple_on_off_on_off,[1,2,1,2],'multiple_on_off_on_off');
test(multiple_on_off_on_off2,[1,2,1,2],'multiple_on_off_on_off2');
test(multiple_on_on,[1,1],'multiple_on_on');

p_multi = p; p_total = p_total + p; p = 0;
f_multi = f; f_total = f_total + f; f = 0;

test(noise_desk,[0],'noise_desk');
test(noise_music,[0],'noise_music');
test(noise_music_on,[0],'noise_music_on');
test(noise_talking,[0],'noise_talking');
test(music_onoffon,[1,2,1],'music_onoffon');
test(noise_background_claps, [0], 'noise_background_claps')
test(noise_talk_whistle, [0], 'noise_talk_whistle')
test(noise_talk_whistle2, [0], 'noise_talk_whistle2')

p_noisy = p; p_total = p_total + p; p = 0;
f_noisy = f; f_total = f_total + f; f = 0;

test(dist_3m,[0],'dist_3m');
test(dist_3m2,[0],'dist_3m2');

p_dist = p; p_total = p_total + p; p = 0;
f_dist = f; f_total = f_total + f; f = 0;

test(alt_clicking,[0],'alt_clicking');
test(alt_drumming,[0],'alt_drumming');
test(alt_finger_drumming, [0], 'alt_finger_drumming')
test(alt_pen_tapping,[0],'alt_pen_tapping');

test(rapid_clapping,[0],'rapid_clapping');
test(rapid_soft_claps,[0],'rapid_soft_claps');

p_alt = p; p_total = p_total + p; p = 0;
f_alt = f; f_total = f_total + f; f = 0;


disp('Results');
disp(sprintf('---------------------------'));
disp(sprintf('- None: %d of %d, %.0f%% % ', p_nothing, p_nothing+f_nothing, 100*p_nothing/(p_nothing+f_nothing)));
disp(sprintf('- Single: %d of %d, %.0f%% % ', p_single, p_single+f_single, 100*p_single/(p_single+f_single)));
disp(sprintf('- Double: %d of %d, %.0f%% % ', p_double, p_double+f_double, 100*p_double/(p_double+f_double)));
disp(sprintf('- Multi: %d of %d, %.0f%% % ', p_multi, p_multi+f_multi, 100*p_multi/(p_multi+f_multi)));
disp(sprintf('- Alt: %d of %d, %.0f%% % ', p_alt, p_alt+f_alt, 100*p_alt/(p_alt+f_alt)));
disp(sprintf('- Noisy: %d of %d, %.0f%% % ', p_noisy, p_noisy+f_noisy, 100*p_noisy/(p_noisy+f_noisy)));
disp(sprintf('- Distance: %d of %d, %.0f%% % ', p_dist, p_dist+f_dist, 100*p_dist/(p_dist+f_dist)));
disp(sprintf('---------------------------'));
disp(sprintf('- Total: %d of %d, %.0f%% % ', p_total, p_total+f_total, 100*p_total/(p_total+f_total)));
disp(sprintf('---------------------------'));
