function clap_order = process_data_set(data,name,fs)

% This will take a data set passed in as data, and will process it, saving
% the images under the name, there is a default frequency fs of 44100.

if nargin < 3
    fs = 44100;
end

clap_order = [];

% Variables

window_size = 1/50;

% Constants
end_entry = 0;
start_entry = 1;

data_split = fs * window_size;

low_energy_array = [];
high_energy_array = [];
energy = [];

% Extract the second data from the measured data, we can ignore the 
dat = data(:,2);

while end_entry < length(data)
    
    end_entry = end_entry + data_split;
    
    % Returns an array of clap times
    [low_band_energy, high_band_energy] = short_window_process(dat(start_entry:end_entry), fs);
    start_entry = start_entry + data_split;
    
    
    low_energy_array = [low_energy_array low_band_energy];
    high_energy_array = [high_energy_array high_band_energy];
    energy = [energy low_band_energy+high_band_energy];
    
end

figure(1)
subplot(2,1,1)
plot(low_energy_array)
subplot(2,1,2)
plot(high_energy_array)
figure(2)
plot(energy)
disp(name)
print('-f1',strcat('images/','interim/',name),'-dpng');
print('-f2',strcat('images/','final/',name),'-dpng');
close all
end

