%
% ELEC4123
% Task2
%
% Luke Exton and Rob Joseph � 2015
%
% Initial code for making the samples for offline processing and testing of
% the framework.

close all;
clear all;
clc;

% DAQ Constants
global VENDOR;
VENDOR = 'ni';
global DAQ_ID;
DAQ_ID = 'nidaq';

BOARD_INDEX=1;


global DEVICE_NAME;
info = daqhwinfo(DAQ_ID);
if length(info.InstalledBoardIds) == 1
    DEVICE_NAME = info.InstalledBoardIds{BOARD_INDEX};
    disp(DEVICE_NAME);
else
    disp('Multiple Boards Detected');
    disp('Unable to determine which board to use');
end

% IO Constants
global IN_CHANNEL;
IN_CHANNEL = 0;
global OUT_CHANNEL;
OUT_CHANNEL = 0;
global HIGH;
HIGH = 5;
global LOW;
LOW = 0;

% DAQ Sampling Details
global WINDOW_SIZE;
WINDOW_SIZE = 10;

global SAMPLE_RATE;
SAMPLE_RATE = 44100;

global NFFT
NFFT = WINDOW_SIZE * SAMPLE_RATE;
%NFFT = 1050; 

global seconds;
seconds = 10;

% Intitialization of DAQ
ai = analoginput(DAQ_ID, DEVICE_NAME);
ao = analogoutput(DAQ_ID, DEVICE_NAME);
addchannel(ai, IN_CHANNEL);
addchannel(ao, OUT_CHANNEL);
ai.SampleRate = SAMPLE_RATE;
ai.SamplesPerTrigger = NFFT;
putsample(ao,HIGH);

% Initialise variables
Stopwatch = 0;
window_index=0;
disp('Starting Sampling');
while  Stopwatch < seconds
	tic
    start(ai);
    [data, time] = getdata(ai);

    window_index = window_index + 1;
    Time(window_index) = Stopwatch;
    
    Stopwatch = Stopwatch + toc;
    disp('Window:');
    disp(Stopwatch);
    
    %figure(window_index);
    figure(1);
    plot(time,data);
    xlabel('time(s)');
    ylabel('Voltage(V)');
    drawnow
end

putsample(ao,LOW);
log = [time,data];


