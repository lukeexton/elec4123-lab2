% New approach to processing the data


function [energy_low,energy_high] = process_short(data, fs)

NFFT = length(data);

% Remove the 0 Hz Frequency Component
Voltage = data - mean(data);

% Convert to frequency Domain
y = fft(Voltage, NFFT);
pow = y.*conj(y);

% Band Pass frequencies
fpass1 = 750;
fstop1 = 950;

fpass2 = 1450;
fstop2 = 1900;

npass1 = round((fpass1/fs) * NFFT);
nstop1 = round((fstop1/fs) * NFFT);

npass2 = round((fpass2/fs) * NFFT);
nstop2 = round((fstop2/fs) * NFFT);

band1 = 1 * pow(npass1:nstop1-1);
band2 = 1 * pow(npass2:nstop2-1);

energy_low = mean(band1);

energy_high = mean(band2);

end
