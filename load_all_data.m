
% Load the data we took from the sample's directory
% This has full hardcoded paths, need to work out how to do it locally.
%

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/alt_clicking.mat','log');
alt_clicking = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/alt_drumming.mat','log');
alt_drumming = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/alt_finger_drumming.mat','log');
alt_finger_drumming = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/alt_pen_tapping.mat','log');
alt_pen_tapping = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/dist_3m.mat','log');
dist_3m = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/dist_3m2.mat','log');
dist_3m2 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/double_clap.mat','log');
double_clap = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/double_clap2.mat','log');
double_clap2 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/double_clap3.mat','log');
double_clap3 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/double_clap4.mat','log');
double_clap4 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/multiple_off_off_off.mat','log');
multiple_off_off_off = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_background_claps.mat','log');
noise_background_claps = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_talk_whistle.mat','log');
noise_talk_whistle = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_talk_whistle2.mat','log');
noise_talk_whistle2 = d.log;

load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/multiple_on_off_on_off.mat','log');
multiple_on_off_on_off = d.log;

load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/multiple_on_off_on_off2.mat','log');
multiple_on_off_on_off2 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/multiple_on_on.mat','log');
multiple_on_on = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/music_onoffon.mat','log');
music_onoffon = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/no_claps.mat','log');
no_claps = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/no_claps2.mat','log');
no_claps2 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_desk.mat','log');
noise_desk = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_music.mat','log');
noise_music = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_music_on.mat','log');
noise_music_on = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/noise_talking.mat','log');
noise_talking = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/rapid_clapping.mat','log');
rapid_clapping = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/rapid_soft_claps.mat','log');
rapid_soft_claps = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/single_clap.mat','log');
single_clap = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/single_clap2.mat','log');
single_clap2 = d.log;

d = load('/Users/lexton/Documents/MATLAB/Elec4123-Lab2/samples/single_clap3.mat','log');
single_clap3 = d.log;
