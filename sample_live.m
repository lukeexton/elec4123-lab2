%
% ELEC4123
% Task2
%
% Luke Exton and Rob Joseph Š 2015
%
% Live Clap detection algorythm, this will run live on a set of data


close all;
clc;

% DAQ Constants
global VENDOR;
VENDOR = 'ni';
global DAQ_ID;
DAQ_ID = 'nidaq';

BOARD_INDEX=1;


global DEVICE_NAME;
info = daqhwinfo(DAQ_ID);
if length(info.InstalledBoardIds) == 1
    DEVICE_NAME = info.InstalledBoardIds{BOARD_INDEX};
    disp(DEVICE_NAME);
else
    disp('Multiple Boards Detected');
    disp('Unable to determine which board to use');
end

% IO Constants
global IN_CHANNEL;
IN_CHANNEL = 0;
global OUT_CHANNEL;
OUT_CHANNEL = 0;
global HIGH;
HIGH = 5;
global LOW;
LOW = 0;

% DAQ Sampling Details
global WINDOW_SIZE;
WINDOW_SIZE = 1/20;

global SAMPLE_RATE;
SAMPLE_RATE = 6400;
% This can be optimized more, but this will give 
% a maximum freqency of 3200hz to work with, and optimize the FFT.
% Next step up 12800 -> Cutoff 6400 Hz
% Next step down 3200 -> Cutoff 1600 Hz
% Next step down 800 -> Cutoff 400 Hz

global NFFT
NFFT = WINDOW_SIZE * SAMPLE_RATE;
% NFFT = 1050; 

E_THRESHOLD = 0.003;


% Intitialization of DAQ
ai = analoginput(DAQ_ID, DEVICE_NAME);
ao = analogoutput(DAQ_ID, DEVICE_NAME);
addchannel(ai, IN_CHANNEL);
addchannel(ao, OUT_CHANNEL);
ai.SampleRate = SAMPLE_RATE;
ai.SamplesPerTrigger = NFFT;
putsample(ao,HIGH);

disp('Starting Sampling');
global MAX_CLAP_DIST;
MAX_CLAP_DIST = 1/5;

e_low = 0;
e_high = 0;
countdown = 0;

% arrays for real-time plots
energy_arr = [];
e_low_arr = [];
e_high_arr = [];

looping = 1;
while looping
    start(ai);
    data = getdata(ai);
    [e_low, e_high] = process_short(data,SAMPLE_RATE);
    
    %energy = e_low + e_high;
    energy = e_low;
    
    
% Real-time plots
%     e_low_arr = [e_low_arr e_low];
%     e_high_arr = [e_high_arr e_high];
%     
%     energy_arr = [energy_arr energy];
%     figure(1);
%     subplot(3,1,1)
%     plot(energy_arr);
%     subplot(3,1,2)
%     plot(e_low_arr)
%     subplot(3,1,3)
%     plot(e_high_arr)
%     
%     figure(2)
%     y = fft(data, NFFT);
%     pow = y.*conj(y);
%     plot(pow);
%     
%     drawnow;
    
    if energy > E_THRESHOLD
        if countdown
            disp('Double Clap Detected');
            putsample(ao,LOW);
            countdown = 0;
            
        else
            disp('Clap Detected. Starting Countdown');
            countdown = MAX_CLAP_DIST;
            
        end
    else
        
        if countdown ~= 0 % verify the countdown has started
            countdown = countdown - WINDOW_SIZE;
        end
        if countdown < 0
            disp('Double Clap Not Detected, Single Clap.');
            putsample(ao,HIGH);
            countdown = 0;
        end
    end
end
    putsample(ao,LOW);

